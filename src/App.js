import React, {Component} from 'react';
import './App.less';
// vendor
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
// compontents
import Home from './components/home';
import Details from './components/details';
import Add from './components/add';

class App extends Component{
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/notes/:id' component={Details} />
          <Route path='/add' component={Add} />
        </Switch>
      </Router>
    );
  }
}

export default App;