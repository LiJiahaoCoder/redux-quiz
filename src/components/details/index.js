import React, { Component } from 'react';
// vendor
import {connect} from 'react-redux';
import {Link, Redirect} from 'react-router-dom';
// action
import {deleteNoteAction} from '../../actions/notesActions';
// componentes
import Header from '../../containers/header';

import './index.less';

class Details extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(method, id) {
    switch (method) {
      case 'delete':
        this.props.deleteNote(id);
        break;
      case 'return':
        this.props.history.goBack();
        break;
      default:
        break;
    }
  }  
  
  render() {
    const id = this.props.match.params.id;
    const note = this.props.notes.notes.filter(note => note.id == id) || [];
    // console.info(note)
    {
      this.props.notes.isDeleted ? <Redirect to='/' /> : null
    }
    return (
      <>
        <Header />
        <div className='detail-container'>
          <section className='side-bar'>
            {
              this.props.notes.notes.map(note => <Link key={note.id} to={`/notes/${note.id}`}>{note.title}</Link>)
            }
          </section>
          <div className='details'>
            <section>
              <h1>{note[0].title}</h1>
              <hr />
              <p>{note[0].description}</p>
            </section>
          </div>
          <section className='button-container'>
            <button className='btn-delete' onClick={() => this.handleClick('delete', id)}>删除</button>
            <button className='btn-return' onClick={() => this.handleClick('return')}>返回</button>
          </section>
        </div>
      </>
    )
  }
}

const mapStateToProps = state => ({
  notes: state.notes
});

const mapDispatchToProps = dispatch => ({
  deleteNote: (id) => dispatch(deleteNoteAction(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Details)