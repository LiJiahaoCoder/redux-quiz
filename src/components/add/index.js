import React, { Component } from 'react';
// vendor
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
// action
import {createNoteAction} from '../../actions/notesActions';

class Add extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      description: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  
  handleChange(event, key) {
    const value = event.target.value;
    this.setState({
      [key]: value
    });
  }

  handleClick(method) {
    const {title, description} = this.state;
    if (method === 'submit') {
      this.props.createNote({title, description});
    } else if(method='cancle') {
      this.props.history.goBack();
    }
  }

  render() {
    const {title, description} = this.state;
    return (
      <div>
        {this.props.notes.isCreated ? <Redirect to='/' /> : null}
        <h2>创建笔记</h2>
        <hr />
        <section>标题</section>
        <section>
          <input value={title} type='text' onChange={e => this.handleChange(e, 'title')} />
        </section>
        <section>正文</section>
        <section>
          <textarea value={description} onChange={e => this.handleChange(e, 'description')}></textarea>
        </section>
        <section>
          <button onClick={() => this.handleClick('submit')}>提交</button>
          <button onClick={() => this.handleClick('cancle')}>取消</button>
        </section>
      </div>
    )
  }
}


const mapStateToProps = state => ({
  notes: state.notes
});

const mapDispatchToProps = dispatch => ({
  createNote: (body) => dispatch(createNoteAction(body))
});

export default connect(mapStateToProps, mapDispatchToProps)(Add)
