import React, { Component } from 'react';
// vendor
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
// container
import Header from '../../containers/header';
// action
import {getNotesAction, resetAction} from '../../actions/notesActions';

import './index.less';

// @connect(
//   state => ({notes: state.notes}),
//   {getNotes}
// )

class Home extends Component {
  constructor(props) {
    super(props);
    this.props.getNotes();
    this.props.reset();
  }
  
  render() {
    return (
      <div>
        <Header />
        <section className='notes-container'>
          {
            this.props.notes.map(item => <section className='note-cover'>
              <Link className='note-link' key={item.id} to={`/notes/${item.id}`}>
                <span>
                  {item.title}
                </span>
              </Link>
            </section>
            )
          }
          <section className='note-cover'>
            <Link className='note-link' to='/add'><span>+</span></Link>
          </section>
        </section>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  notes: state.notes.notes
});

const mapDispatchToProps = dispatch => ({
  getNotes: () => dispatch(getNotesAction()),
  reset: () => dispatch(resetAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
