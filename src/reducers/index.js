import {combineReducers} from "redux";
// reducers
import notesReducer from './notesReducers';

const reducers = combineReducers({
  notes: notesReducer
});
export default reducers;