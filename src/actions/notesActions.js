// utils
import {getData, createNote, deleteNote} from '../utils/fetchData';
import {GET_NOTES_URL, CREATE_NOTE_URL, DELETE_NOTES_URL} from '../utils/data';
// action type
const GET_NOTES = 'GET_NOTES';
const CREATE_NOTE = 'CREATE_NOTE';
const DELETE_NOTE = 'DELETE_NOTE';
const RESET = 'RESET';

export const getNotesAction = () => dispatch => {
  getData(GET_NOTES_URL).then(data => {
    dispatch({
      type: GET_NOTES,
      payload: data
    });
  }).catch(err => new Error(err));
};

export const createNoteAction = (body) => dispatch => {
  createNote(CREATE_NOTE_URL, body)
    .then(response => {
      if(response.status === 201) {
        getData(GET_NOTES_URL).then(data => {
          dispatch({
            type: CREATE_NOTE,
            payload: data,
            isCreated: true
          });
        }).catch(err => new Error(err));
      }
    })
};

export const deleteNoteAction = (id) => dispatch => {
  deleteNote(DELETE_NOTES_URL, id)
    .then(response => {
      if(response.status === 200) {
        getData(GET_NOTES_URL).then(data => {
          dispatch({
            type: DELETE_NOTE,
            payload: data,
            isDeleted: true
          });
        }).catch(err => new Error(err));
      }
    });
};

export const resetAction = () => ({
  type: RESET,
  payload: {
    isCreated: false,
    isDeleted: false
  }
});
