const BASE_URL = 'http://localhost:8080/api';

const GET_NOTES_URL = `${BASE_URL}/posts`;
const CREATE_NOTE_URL = `${BASE_URL}/posts`;
const DELETE_NOTES_URL = `${BASE_URL}/posts`;

export {
  GET_NOTES_URL,
  DELETE_NOTES_URL,
  CREATE_NOTE_URL
};