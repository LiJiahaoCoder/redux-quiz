import React from 'react';
import './index.less';

import {MdEventNote} from 'react-icons/md';

export default function Header() {
  return (
    <header>
      <MdEventNote />
      <span>NOTES</span>
    </header>
  );
}